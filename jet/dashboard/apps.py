from django.apps import AppConfig


class JetDashboardConfig(AppConfig):
    name = "jet.dashboard"
    default_auto_field = "django.db.models.AutoField"
